# Documentation

1. [INSTALLATION](#1-installation)
    1. Get file
    2. Include file
    3. Add dependency
    4. Add plugin
2. [API KEY](#2-api-key)
    1. Generate your `api_key` at weatherforecast
    2. Insert your `api_key` into `aping-config.js`
open demo/index.html for weather forecast